package de.cubenation.bungee.listener;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import de.cubenation.bungee.CNBungeeJoinPlugin;
import de.cubenation.bungee.model.PlayerServer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by BenediktHr on 29.09.15.
 * Project: CNBungeeJoin
 * Package: de.cubenation.bungee.listener
 */
public class PlayerListener implements Listener {

    @EventHandler
    public void onServerSwitchEvent(final ServerSwitchEvent event) throws InterruptedException {
        final CNBungeeJoinPlugin plugin = CNBungeeJoinPlugin.getInstance();


        if (plugin.getList() != null) {
            PlayerServer pl = null;

            for (PlayerServer playerServer : plugin.getList()) {
                if (playerServer.getUuid().equals(event.getPlayer().getUniqueId())) {

                    plugin.getLogger().info("       #######################");
                    plugin.getLogger().info("       SWITCH");
                    plugin.getLogger().info("       player: " + event.getPlayer().getDisplayName());
                    plugin.getLogger().info("       from: " + playerServer.getSourceServer());
                    plugin.getLogger().info("       to: " + event.getPlayer().getServer().getInfo().getName());
                    plugin.getLogger().info("       #######################");




                    ByteArrayDataOutput from = ByteStreams.newDataOutput();
                    from.writeUTF("switchFrom");
                    from.writeUTF(event.getPlayer().getUniqueId().toString());

                    playerServer.getSourceServer().sendData("CNBungeeJoin", from.toByteArray());


                    ByteArrayDataOutput to = ByteStreams.newDataOutput();

                    to.writeUTF("switchTo");
                    to.writeUTF(event.getPlayer().getUniqueId().toString());

                    event.getPlayer().getServer().getInfo().sendData("CNBungeeJoin", to.toByteArray());


                    pl = playerServer;
                    break;
                }
            }
            if (pl != null) {
                plugin.getList().remove(pl);
            }
        }



    }

    @EventHandler
    public void onServerConnectedEvent(ServerConnectedEvent event) {
        final CNBungeeJoinPlugin plugin = CNBungeeJoinPlugin.getInstance();

        plugin.getList().add(
                new PlayerServer(event.getPlayer().getUniqueId(),
                        event.getPlayer().getServer().getInfo()));

    }

    @EventHandler
    public void onLocalPlayerDisconnect(PlayerDisconnectEvent event) {
        final CNBungeeJoinPlugin plugin = CNBungeeJoinPlugin.getInstance();
        plugin.getLogger().info("           PlayerDisconnectEvent");
        plugin.getLogger().info(event.getPlayer().getServer().getInfo().getName());

    }

    @EventHandler
    public void onPostLoginEvent(PostLoginEvent event) {
        CNBungeeJoinPlugin plugin = CNBungeeJoinPlugin.getInstance();
        plugin.getLogger().info("           onPostLoginEvent");
        try {
            plugin.getLogger().info("           server" + event.getPlayer().getServer().getInfo().getName());
        } catch (Exception ignored) {

        }

    }

    @EventHandler
    public void onPlayerDisconnectEvent(PlayerDisconnectEvent event) {
        CNBungeeJoinPlugin plugin = CNBungeeJoinPlugin.getInstance();

        plugin.getLogger().info("           onPlayerDisconnectEvent");
        plugin.getLogger().info("           server" + event.getPlayer().getServer().getInfo().getName());


        ByteArrayDataOutput to = ByteStreams.newDataOutput();

        to.writeUTF("disconnect");
        to.writeUTF(event.getPlayer().getUniqueId().toString());

        event.getPlayer().getServer().getInfo().sendData("CNBungeeJoin", to.toByteArray());


    }
}
