package de.cubenation.bungee;

import de.cubenation.bungee.listener.PlayerListener;
import de.cubenation.bungee.model.PlayerServer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.ArrayList;

/**
 * Created by BenediktHr on 29.09.15.
 * Project: CNBungeeJoin
 * Package: de.cubenation.bungee
 */
public class CNBungeeJoinPlugin extends Plugin {

    public static CNBungeeJoinPlugin instance;
    private ArrayList<PlayerServer> list;

    public static CNBungeeJoinPlugin getInstance() {
        return instance;
    }

    public static void setInstance(CNBungeeJoinPlugin instance) {
        CNBungeeJoinPlugin.instance = instance;
    }

    @Override
    public void onEnable() {
        setInstance(this);
        getLogger().info("It loads!");

        this.list = new ArrayList<PlayerServer>();

        this.getProxy().getPluginManager().registerListener(this, new PlayerListener());
    }

    public ArrayList<PlayerServer> getList() {
        return list;
    }
}
