package de.cubenation.bungee.bridge;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.command.AbstractCommand;
import de.cubenation.bungee.bridge.config.locale.de_DE;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by BenediktHr on 03.10.15 at 22:53.
 * Project: CNBungeeJoin
 * Package: de.cubenation.bungee.bridge
 */
public class CNBungeeJoinPlugin extends BasePlugin implements PluginMessageListener {

    private static CNBungeeJoinPlugin instance;

    public static CNBungeeJoinPlugin getInstance() {
        return instance;
    }

    public static void setInstance(CNBungeeJoinPlugin instance) {
        CNBungeeJoinPlugin.instance = instance;
    }

    @Override
    protected void onPreEnable() throws Exception {
        setInstance(this);
    }

    @Override
    protected void onPostEnable() throws Exception {
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "CNBungeeJoin");
        this.getServer().getMessenger().registerIncomingPluginChannel(this, "CNBungeeJoin", this);
    }

    @Override
    public HashMap<String, String> getCustomConfigurationFiles() {
        return new HashMap<String, String>() {{
            put(de_DE.getFilename(), de_DE.class.getName());
        }};
    }

    @Override
    public HashMap<String, ArrayList<AbstractCommand>> getCommands() {
        return null;
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] bytes) {

        getLogger().info("Received Bungee MSG!");


        if (!channel.equals("CNBungeeJoin")) {
            return;
        }
        ByteArrayDataInput in = ByteStreams.newDataInput(bytes);
        String subchannel = in.readUTF();

        if (subchannel.equals("switchTo")) {
            UUID uuid = UUID.fromString(in.readUTF());

            getLogger().info("       #######################");
            getLogger().info("       switchTo");
            getLogger().info("       player: " + uuid);
            getLogger().info("       #######################");
        } else if (subchannel.equals("switchFrom")) {
            UUID uuid = UUID.fromString(in.readUTF());

            getLogger().info("       #######################");
            getLogger().info("       switchFrom");
            getLogger().info("       player: " + uuid);
            getLogger().info("       #######################");
        } else if (subchannel.equals("disconnect")) {
            UUID uuid = UUID.fromString(in.readUTF());
            sendToAll(uuid, "disconnect");
        }

    }

    private void sendToAll(UUID uuid, String string) {
        Player player = Bukkit.getServer().getPlayer(uuid);
        for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
            if (string.equals("disconnect")) {
                pl.sendMessage("[-] " + player.getName());
            }

        }
        CNBungeeJoinPlugin.getInstance().getLogger().info("[-] " + player.getName());
    }
}
