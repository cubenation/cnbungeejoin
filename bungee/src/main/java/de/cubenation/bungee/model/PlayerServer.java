package de.cubenation.bungee.model;

import net.md_5.bungee.api.config.ServerInfo;

import java.util.UUID;

/**
 * Created by BenediktHr on 03.10.15.
 */
public class PlayerServer {

    private UUID uuid;

    private ServerInfo sourceServer;


    public PlayerServer(UUID uuid, ServerInfo sourceServer) {
        this.uuid = uuid;
        this.sourceServer = sourceServer;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public ServerInfo getSourceServer() {
        return sourceServer;
    }

    public void setSourceServer(ServerInfo sourceServer) {
        this.sourceServer = sourceServer;
    }

}
