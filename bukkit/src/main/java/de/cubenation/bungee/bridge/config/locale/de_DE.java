package de.cubenation.bungee.bridge.config.locale;

import de.cubenation.bedrock.BasePlugin;
import de.cubenation.bedrock.service.config.CustomConfigurationFile;

import java.io.File;

/**
 * Created by BenediktHr on 03.10.15 at 22:59.
 * Project: CNBungeeJoin
 * Package: de.cubenation.bungee.bridge.config.locale
 */
public class de_DE extends CustomConfigurationFile {

    public static String getFilename() {
        return "locale" + File.separator + "de_DE.yml";
    }

    public de_DE(BasePlugin plugin, String filename) {
        CONFIG_FILE = new File(plugin.getDataFolder(), filename);
    }

    public de_DE(BasePlugin plugin) {
        this(plugin, getFilename());
    }


}
